# Register your models here.
from django.contrib import admin
from app.models import event,userinfo
from django.contrib.auth.models import User
# Register your models here.
class eventinline(admin.StackedInline):
        model=event
        extra=3
class useradmin(admin.ModelAdmin):
#        fieldsets=[('user',{'fields':['user']}),
#		('Email Status',{'fields':['email_confirmed']}),
 #               ]
        inlines=[eventinline]
admin.site.register(userinfo,useradmin)
