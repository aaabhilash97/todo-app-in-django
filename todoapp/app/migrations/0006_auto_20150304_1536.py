# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0005_remove_userinfo_reg_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='userinfo',
            name='gender',
            field=models.CharField(default=b'None', max_length=200),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userinfo',
            name='link',
            field=models.CharField(default=b'None', max_length=200),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userinfo',
            name='propicurl',
            field=models.CharField(default=b'None', max_length=200),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='userinfo',
            name='provider',
            field=models.CharField(default=b'None', max_length=200),
            preserve_default=True,
        ),
    ]
