# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_userinfo_email_confirmed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userinfo',
            name='email_confirmed',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
