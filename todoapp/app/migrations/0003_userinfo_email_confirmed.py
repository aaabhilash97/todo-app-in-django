# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0002_remove_userinfo_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='userinfo',
            name='email_confirmed',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
