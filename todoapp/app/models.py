from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
import datetime
# Create your models here.
class userinfo(models.Model):
        user = models.ForeignKey(User, unique=True)
	email_confirmed=models.BooleanField(default=False)
	provider=models.CharField(max_length=200,default='None')
	propicurl=models.CharField(max_length=200,default='None')
	link=models.CharField(max_length=200,default='None')
	gender=models.CharField(default='None',max_length=200)
        def __str__(self):
               return self.user.username

class event(models.Model):
        user=models.ForeignKey(userinfo)
        name=models.CharField(max_length=20)
        start_date=models.DateTimeField('start date')
        end_date=models.DateTimeField('end date')
        description=models.CharField(max_length=200)
        def __str__(self):
                return self.name
        def is_gng_end(self):
                status=self.end_date - timezone.now() <=datetime.timedelta(days=1) 
		if self.end_date - timezone.now() <=datetime.timedelta(days=0):
			return "event completed"
		if status:
			return "less than 1 day remaining"
		else:
			return "more days"
