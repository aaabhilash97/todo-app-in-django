from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse,Http404,HttpResponseRedirect
from app.models import userinfo
from django.template import RequestContext,loader
from django.core.urlresolvers import reverse
from django.views import generic
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from datetime import datetime
from django.conf import settings
from app.oauth import OAuthSignIn
from .slogin import slogin
# Create your views here.
#Authorization
def oauth_authorize(request,provider):
     try:
    	if request.user.is_authenticated() and request.method=='GET':
        	raise ValueError
    	oauth = OAuthSignIn.get_provider(provider)
    	return oauth.authorize()
     except:
	return HttpResponse("<script>window.opener.location.reload(true);window.close();</script>")
#Callback
def oauth_callback(request,provider):
    if request.user.is_authenticated() and request.method=='GET':
                return HttpResponseRedirect('/app')
    if not request.user.is_anonymous():
        return redirect(url_for('index'))
    oauth = OAuthSignIn(provider)
    oauth = OAuthSignIn.get_provider(provider)
    if provider=='google' or provider=='facebook':
		code=request.GET.get('code')
    elif provider=='twitter':
		code=request.GET.get('oauth_verifier')
    me = oauth.callback(code)
    auth.login(request,slogin(me))
    return HttpResponse("<script>window.opener.location.reload(true);window.close();</script>")

#Logut
def logout(request):
	auth.logout(request)
	return HttpResponseRedirect('/app/login')
def login(request):
	if request.user.is_authenticated() and request.method=='GET':
		return HttpResponseRedirect(request.GET.get('next') or '/app')
	if request.method=='POST':
		try:
			username=request.POST['username']
			password=request.POST['password']
			user=auth.authenticate(username=username,password=password)
			auth.login(request,user)
			try:
			    if request.POST['rememberme']:
				None
			except:
				request.session.set_expiry(0)
			return HttpResponseRedirect(request.GET.get('next') or'/app')
		except:
			return render(request,'app/login.html',{'error':"invalid user"})
	else:
		return render(request,'app/login.html',{})
@login_required(login_url='/app/login')
def index(request):
	if request.method=='POST':
		ename=request.POST['eventname']
		edate=request.POST['eventdate']
		edesc=request.POST['eventdescription']
		try:
			ui=request.user.userinfo_set.get(user=request.user)
		except:
			ui=request.user.userinfo_set.create(user=request.user)
	#	e=ui.event_set.create(name=ename,start_date=datetime.now(),end_date=edate,description=edesc)

	ui=request.user
	try:
		ui=ui.userinfo_set.get(user=ui)
		event=ui.event_set.all()
	except:
		event=None
	return render(request,'app/index.html',{'event':event,'ui':ui})
def register(request):
	if request.user.is_authenticated() and request.method=='GET':
                return HttpResponseRedirect(request.GET.get('next') or '/app')
	try:
		if request.POST['register']:
			try:
				username=request.POST['username']
				email=request.POST['email']
				password=request.POST['password']
				firstname=request.POST['first_name']
				lastname=request.POST['last_name']
				user=User.objects.create_user(username=username,email=email,password=password,first_name=firstname,last_name=lastname)
				ui=user.userinfo_set.create(user=user)
				return render(request,'app/registration.html',{'submit':"user registered successfully"})
			except:
				return render(request,'app/registration.html',{'submit':"username already in use"})
	except:
		return render(request,'app/registration.html',{})
